package com.dragonfly;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.model.Fixtures;
import com.dragonfly.model.ORM;
import com.j256.ormlite.logger.LocalLog;


public class Main
{
    public static void main(String[] args) throws Throwable {
        System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "WARNING");
        ORM.init("jdbc:mysql://localhost:3306/dragonfly", "root", "bagpipe");

        Kernel.execute("Security", "loginForm", null);

//        Fixtures.load();

//        ArgumentCollection response = Kernel.execute("Vol", "createOrUpdateForm", null);
//        Kernel.execute("Vol", "createOrUpdate", response);
        Kernel.execute("Vol", "display", null);
    }
}
