package com.dragonfly.model;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.model.dao.AvionMarqueDao;
import com.dragonfly.model.dao.AvionModeleDao;
import com.dragonfly.model.dao.AvionTypeDao;
import com.dragonfly.model.dao.FonctionDao;
import com.dragonfly.model.entity.*;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

public class Fixtures
{
    public static void load() throws Throwable {
        Fixtures.buildSchema();
        Fixtures.initData();
    }

    private static void buildSchema() throws Exception {
        TableUtils.createTableIfNotExists(ORM.getInstance(), Role.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), User.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), Airport.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), AirportPiste.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), AvionMarque.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), AvionType.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), AvionModele.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), Avion.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), Vol.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), Fonction.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), Personne.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), LnkPersonneVol.class);
        TableUtils.createTableIfNotExists(ORM.getInstance(), LnkUserRole.class);
    }

    private static void initData() throws Throwable
    {
        Dao<Role, Integer> roleDao = ORM.getDao(Role.class);
        for(String[] r : new String[][] {{Role.ROLE_ADMIN, "Administrateur"}, {Role.ROLE_MANAGER, "Manager"}}) {
            Role role = new Role();
            role.setRole(r[0]);
            role.setLabel(r[1]);
            roleDao.createOrUpdate(role);
        }

        FonctionDao fonctionDao = ORM.getDao(Fonction.class);
        for (String s : new String[] {"pilote", "copilote", "PNC"}) {
            Fonction f = new Fonction();
            f.setLabel(s);
            fonctionDao.createOrUpdate(f);
        }

        // Marque & modeles
        AvionType[] types = new AvionType[] {
                new AvionType("Long courier"),
                new AvionType("Moyen courier")
        };
        AvionMarque[] marques = new AvionMarque[] {
                new AvionMarque("Boeing"),
                new AvionMarque("Airbus")
        };
        AvionModele[] modeles = new AvionModele[] {
                new AvionModele("747",  marques[0], types[0], 3, 4),
                new AvionModele("A380", marques[1], types[1], 2, 3)
        };
        Dao<AvionModele, Integer> avionModeleDao = ORM.getDao(AvionModele.class);
        for (AvionModele am : modeles) {
            AvionModele m = avionModeleDao.queryBuilder().where().eq(AvionModele.COL_NOM, am.getNom()).queryForFirst();
            if (m == null)
                avionModeleDao.create(am);
        }

        Kernel.execute("AirportPiste", "import", new ArgumentCollection(new Object[] {"file", "airports.ser"}));

        Dao<Vol, String> volDao = ORM.getDao(Vol.class);

        AvionMarqueDao avionMarqueDao = ORM.getDao(AvionMarque.class);

        AvionTypeDao avionTypeDao = ORM.getDao(AvionType.class);
        AvionModele b757 = avionModeleDao.queryBuilder().where().eq(AvionModele.COL_NOM, "757").queryForFirst();
        if (b757 == null) {
            b757 = new AvionModele("757", avionMarqueDao.findByNom("Boeing"), avionTypeDao.findByLabel("Long courier"), 10, 20);
            avionModeleDao.createOrUpdate(b757);
        }

        Dao<Avion, Integer> avionDao = ORM.getDao(Avion.class);
        Avion avion = new Avion();
        avion.setModele(b757);
        avionDao.createOrUpdate(avion);

        Dao<AirportPiste, Integer> airportPisteDao = ORM.getDao(AirportPiste.class);
        Vol vo = new Vol();
        vo.setId("666");
        vo.setAvion(avion);
        vo.setPisteDepart (airportPisteDao.queryBuilder().where().eq(AirportPiste.COL_LABEL, "CDG 09R/27L").queryForFirst());
        vo.setPisteArrivee(airportPisteDao.queryBuilder().where().eq(AirportPiste.COL_LABEL, "EDI 06/24").queryForFirst());
        vo.setDateDepart (new SimpleDateFormat("dd/mm/yyyy").parse("21/04/2009"));
        vo.setDateArrivee(new SimpleDateFormat("dd/mm/yyyy").parse("22/04/2009"));
        volDao.createOrUpdate(vo);

        Dao<Personne, Integer> personneDao = ORM.getDao(Personne.class);
        Dao<LnkPersonneVol, Integer> lnkPersonneVolsDao = ORM.getDao(LnkPersonneVol.class);
        String[][] equipageData = new String[][] {
                {"Dickinson", "Bruce",   "pilote"},
                {"???",       "???",     "copilote"},
                {"Harris",    "Steve",   "PNC"},
                {"Murray",    "Dave",    "PNC"},
                {"Adrian",    "Smith",   "PNC"},
                {"Nicko",     "McBrain", "PNC"},
                {"Janick",    "Gers",    "PNC"},
        };
        for(String[] data : equipageData)
        {
            Personne p = new Personne(data[0], data[1]);
            personneDao.create(p);
            LnkPersonneVol pv= new LnkPersonneVol(p, vo, fonctionDao.findByLabel(data[2]));
            lnkPersonneVolsDao.create(pv);
        }
    }
}
