package com.dragonfly.model.dao;

import com.dragonfly.model.entity.AvionModele;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class AvionModeleDao extends BaseDaoImpl<AvionModele, String>
{
    public AvionModeleDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AvionModele.class);
    }

    public AvionModele findByNom(String nom) throws SQLException
    {
        return this.queryBuilder().where().eq(AvionModele.COL_NOM, nom).queryForFirst();
    }
}
