package com.dragonfly.model.dao;

import com.dragonfly.model.entity.AvionType;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class AvionTypeDao  extends BaseDaoImpl<AvionType, String>
{
    public AvionTypeDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AvionType.class);
    }

    public AvionType findByLabel(String label) throws SQLException {
        return this.queryBuilder().where().eq(AvionType.COL_LABEL, label).queryForFirst();
    }
}
