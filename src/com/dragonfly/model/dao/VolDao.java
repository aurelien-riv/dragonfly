package com.dragonfly.model.dao;

import com.dragonfly.model.entity.Vol;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class VolDao extends BaseDaoImpl<Vol, String>
{
    public VolDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Vol.class);
    }

    public List<Vol> find(long from, long limit, String order, Object[] ... criterias) throws Exception
    {
        Where<Vol, String> qb = this.queryBuilder().orderBy(order, true).offset(from).limit(limit)
                .where().raw("1 = 1"); // Ensure that we have at least one condition for the where statement

        if (criterias != null) {
            for(Object[] criteria : criterias) {
                if (criteria.length == 2)
                    qb.and().eq((String) criteria[0], criteria[1]);
                else
                    throw new IllegalArgumentException();
            }
        }
        return qb.query();
    }
}
