package com.dragonfly.model.dao;

import com.dragonfly.model.entity.AvionMarque;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class AvionMarqueDao extends BaseDaoImpl<AvionMarque, String>
{
    public AvionMarqueDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, AvionMarque.class);
    }

    public AvionMarque findByNom(String nom) throws SQLException
    {
        return this.queryBuilder().where().eq(AvionMarque.COL_NOM, nom).queryForFirst();
    }
}
