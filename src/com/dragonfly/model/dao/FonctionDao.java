package com.dragonfly.model.dao;

import com.dragonfly.model.entity.Fonction;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;

public class FonctionDao extends BaseDaoImpl<Fonction, Integer> {
    public FonctionDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Fonction.class);
    }

    public Fonction findByLabel(String label) throws SQLException
    {
        return this.queryBuilder().where().eq(Fonction.COL_LABEL, label).queryForFirst();
    }
}
