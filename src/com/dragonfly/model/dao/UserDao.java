package com.dragonfly.model.dao;

import com.dragonfly.lib.PasswordEncoder;
import com.dragonfly.model.entity.User;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class UserDao extends BaseDaoImpl<User, Integer>
{
    public UserDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, User.class);
    }

    public User findByLoginAndPassword(String login, String password) throws SQLException {
        return this.queryBuilder().where()
                .eq(User.COL_LOGIN, login)
                .and().eq(User.COL_PASSWORD, password)
                .queryForFirst();
    }
}