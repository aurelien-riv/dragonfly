package com.dragonfly.model;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;

import java.sql.SQLException;

public final class ORM {
    private static JdbcConnectionSource instance;

    private ORM() {

    }

    public static <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException
    {
        return DaoManager.createDao(ORM.instance, clazz);
    }

    public static void init(String uri, String username, String password) throws Exception {
        if (instance != null)
            throw new Exception("ORM already initialized");
        instance = new JdbcConnectionSource(uri, username, password);
    }

    public static JdbcConnectionSource getInstance() {
        return instance;
    }
}
