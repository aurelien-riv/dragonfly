package com.dragonfly.model.entity;

import com.dragonfly.model.dao.AvionTypeDao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@DatabaseTable(tableName = "avion_type", daoClass = AvionTypeDao.class)
public class AvionType
{
    public static final String COL_LABEL = "label";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = COL_LABEL)
    private String label;

    @ForeignCollectionField()
    private ForeignCollection<AvionModele> modeles;

    public AvionType() {
    }

    public AvionType(String label) {
        this.label = label;
    }
}
