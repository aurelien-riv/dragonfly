package com.dragonfly.model.entity;

import com.dragonfly.lib.PasswordEncoder;
import com.dragonfly.model.dao.UserDao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

@Getter @Setter
@DatabaseTable(tableName = "user", daoClass = UserDao.class)
public class User implements com.dragonfly.kernel.User
{
    public static final String COL_ID       = "id";
    public static final String COL_LOGIN    = "login";
    public static final String COL_PASSWORD = "password";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true, columnName = COL_ID)
    private int id;

    @DatabaseField(columnName = COL_LOGIN, unique = true)
    String login;

    @DatabaseField(columnName = COL_PASSWORD)
    String password;

    @ForeignCollectionField
    private ForeignCollection<LnkUserRole> lnkRoles;

    public User()
    {
    }

    public User(String login, String password)
    {
        this.login = login;
        this.setPassword(password);
    }

    public Collection<String> getRoles()
    {
        Collection<String> roles = new ArrayList<String>(this.lnkRoles.size());
        for(LnkUserRole lUR : this.lnkRoles)
            roles.add(lUR.getRole().getRole());
        return roles;
    }

    public void setPassword(String password)
    {
        this.password = PasswordEncoder.hash(password);
    }
}
