package com.dragonfly.model.entity;

import com.dragonfly.model.dao.FonctionDao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "fonction", daoClass = FonctionDao.class)
public class Fonction
{
    public static final String PILOTE = "pilote";

    public static final String COL_LABEL = "label";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = COL_LABEL, unique = true)
    private String label;

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
