package com.dragonfly.model.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "personne")
public class Personne
{
    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String nom;

    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String prenom;

    @ForeignCollectionField()
    private ForeignCollection<LnkPersonneVol> vols;

    public Personne() {}

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }
}
