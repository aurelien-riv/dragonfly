package com.dragonfly.model.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "role")
public class Role
{
    public static final String ROLE_ADMIN   = "ADMIN";
    public static final String ROLE_MANAGER = "MANAGER";

    public static final String COL_ID = "id";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true, columnName = COL_ID)
    private int id;

    @DatabaseField
    private String role;

    @DatabaseField
    private String label;

    @ForeignCollectionField
    private ForeignCollection<LnkUserRole> lnkUsers;
}
