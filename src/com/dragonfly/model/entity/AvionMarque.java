package com.dragonfly.model.entity;

import com.dragonfly.model.dao.AvionMarqueDao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "avion_marque", daoClass = AvionMarqueDao.class)
public class AvionMarque
{
    public static final String COL_NOM = "nom";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = COL_NOM, unique = true)
    private String nom;

    @ForeignCollectionField()
    private ForeignCollection<AvionModele> modeles;

    public AvionMarque() {}

    public AvionMarque(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return nom;
    }
}
