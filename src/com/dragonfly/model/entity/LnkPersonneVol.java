package com.dragonfly.model.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "lnk_personne_vol")
public class LnkPersonneVol
{
    public static final String COL_VOLID = "vol_id";
    public static final String COL_FONCTIONID = "fonction_id";
    public static final String COL_PERSONNEID = "personne_id";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = COL_PERSONNEID)
    private Personne personne;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = COL_VOLID)
    private Vol vol;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = COL_FONCTIONID)
    private Fonction fonction;

    public LnkPersonneVol() {}

    public LnkPersonneVol(Personne personne, Vol vol, Fonction fonction) {
        this.personne = personne;
        this.vol = vol;
        this.fonction = fonction;
    }
}
