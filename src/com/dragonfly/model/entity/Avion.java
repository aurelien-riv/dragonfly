package com.dragonfly.model.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "avion")
public class Avion
{
    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, canBeNull = false)
    private AvionModele modele;

    @ForeignCollectionField()
    private ForeignCollection<Vol> vols;

    @Override
    public String toString() {
        return "Avion{" +
                "id=" + id +
                ", modele=" + modele +
                '}';
    }
}
