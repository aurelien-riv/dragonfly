package com.dragonfly.model.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter @Setter
@DatabaseTable(tableName = "airport_piste")
public class AirportPiste implements Serializable
{
    private static final long serialVersionUID = -6283504539408473149L;

    public static final String COL_LABEL = "label";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true,  foreignAutoRefresh = true)
    private Airport airport;

    @DatabaseField(columnName = COL_LABEL)
    private String label;

    @Override
    public String toString() {
        return label;
    }
}
