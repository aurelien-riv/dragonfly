package com.dragonfly.model.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "lnk_user_role")
public class LnkUserRole
{
    public static final String COL_USERID = "user_id";
    public static final String COL_ROLEID = "role_id";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = COL_USERID, foreignColumnName = User.COL_ID)
    private User user;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = COL_ROLEID, foreignColumnName = Role.COL_ID)
    private Role role;
}
