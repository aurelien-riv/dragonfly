package com.dragonfly.model.entity;

import com.dragonfly.model.dao.AvionModeleDao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "avion_modele", daoClass = AvionModeleDao.class)
public class AvionModele
{
    public static final String COL_NOM = "nom";

    @Setter(AccessLevel.PRIVATE)
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(unique = true, columnName = COL_NOM, canBeNull = false)
    private String nom;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, canBeNull = false)
    private AvionMarque marque;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, canBeNull = false)
    private AvionType type;

    @DatabaseField()
    private int nbPncMin;

    @DatabaseField()
    private int nbPncMax;

    @ForeignCollectionField()
    private ForeignCollection<Avion> avions;

    public AvionModele() {}

    public AvionModele(String nom, AvionMarque marque, AvionType type, int nbPncMin, int nbPncMax) {
        this.nom = nom;
        this.marque = marque;
        this.type = type;
        this.nbPncMin = nbPncMin;
        this.nbPncMax = nbPncMax;
    }

    @Override
    public String toString() {
        return "AvionModele{" +
                "nom='" + nom + '\'' +
                ", marque=" + marque +
                ", nbPncMin=" + nbPncMin +
                ", nbPncMax=" + nbPncMax +
                '}';
    }
}
