package com.dragonfly.model.entity;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.dragonfly.model.ORM;
import com.dragonfly.model.dao.FonctionDao;
import com.dragonfly.model.dao.VolDao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@DatabaseTable(tableName = "vol", daoClass = VolDao.class)
public class Vol
{
    public static final String COL_PISTE_DEPART  = "piste_depart";
    public static final String COL_PISTE_ARRIVEE = "piste_arrivee";
    public static final String COL_DATE_DEPART   = "date_depart";
    public static final String COL_DATE_ARRIVEE  = "date_arrivee";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private Avion avion;

    @ForeignCollectionField()
    private ForeignCollection<LnkPersonneVol> equipage;

    @DatabaseField(columnName = COL_PISTE_DEPART, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private AirportPiste pisteDepart;

    @DatabaseField(columnName = COL_PISTE_ARRIVEE, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private AirportPiste pisteArrivee;

    @DatabaseField(columnName = COL_DATE_DEPART, canBeNull = false)
    private Date dateDepart;

    @DatabaseField(columnName = COL_DATE_ARRIVEE, canBeNull = false)
    private Date dateArrivee;

    public List<Personne> findEquipageForFonction(Fonction fonction) throws SQLException
    {
        QueryBuilder lnkPersonneVolQB = ORM.getDao(LnkPersonneVol.class).queryBuilder();
        lnkPersonneVolQB.where()
                .eq(LnkPersonneVol.COL_VOLID,      this.id)
                .and()
                .eq(LnkPersonneVol.COL_FONCTIONID, fonction.getId());

        return ORM.getDao(Personne.class).queryBuilder().join(lnkPersonneVolQB).query();
    }

    public boolean isValid() throws SQLException
    {
        int nbPnc = this.getEquipage().size();
        if (this.avion.getModele().getNbPncMin() > nbPnc)
            return false;
        if (this.avion.getModele().getNbPncMax() < nbPnc)
            return false;
        FonctionDao fnDao = ORM.getDao(Fonction.class);
        if (this.findEquipageForFonction(fnDao.findByLabel(Fonction.PILOTE)).isEmpty())
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Vol{" +
                "id='" + id + '\'' +
                ", pisteDepart=" + pisteDepart +
                ", pisteArrivee=" + pisteArrivee +
                ", dateDepart=" + dateDepart +
                ", dateArrivee=" + dateArrivee +
                '}';
    }
}
