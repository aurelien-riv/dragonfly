package com.dragonfly.view.swing;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class VolTableModel extends AbstractTableModel
{
    private List<String> columnNames = new ArrayList();
    private List<List<String>> data = new ArrayList();
    {
        columnNames.add("Avion");
        columnNames.add("Modèle");
        columnNames.add("Date Départ");
        columnNames.add("Aeroport Départ");
        columnNames.add("Date Arrivée");
        columnNames.add("Aeroport Arrivée");
        columnNames.add("Nom");
        columnNames.add("Prenom");
        columnNames.add("Fonction");
    }

    public void addRow(List<String> rowData) {
        data.add(rowData);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    public int getColumnCount() {
        return columnNames.size();
    }

    public int getRowCount() {
        return data.size();
    }

    public String getColumnName(int col) {
        try {
            return columnNames.get(col);
        } catch (Exception e) {
            return null;
        }
    }

    public Object getValueAt(int row, int col) {
        return data.get(row).get(col);
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
}
