package com.dragonfly.view.swing;

import com.dragonfly.controller.SecurityController;
import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.kernel.View;
import com.dragonfly.lib.PasswordEncoder;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SecurityView extends View
{
    public void loginFormTemplate(ArgumentCollection args) throws Throwable
    {
        final JDialog dialog = new JDialog((JDialog) null, "Connexion");
        final JTextField tfUsername = new JTextField(20);
        final JPasswordField pfPassword = new JPasswordField(20);

        //region actionListeners
        ActionListener onConfirmAction = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String login = tfUsername.getText().trim();
                String password = PasswordEncoder.hash(new String(pfPassword.getPassword()));
                try {
                    Kernel.execute("Security", "login", new ArgumentCollection(new Object[][]{
                            {"login", login},
                            {"#password", password}
                    }));
                    dialog.dispose();
                } catch (SecurityController.InvalidCredentialsException e) {
                    JOptionPane.showMessageDialog(dialog, "Login ou mot de passe invalide", "Erreur", JOptionPane.ERROR_MESSAGE);
                    tfUsername.setText("");
                    pfPassword.setText("");
                } catch (Throwable t) {
                    t.printStackTrace();
                    System.exit(-1);
                }
            }
        };
        ActionListener onCancelAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        };
        //endregion

        //region GUI
        JPanel panel = new JPanel(new GridBagLayout());

        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;

        //region input
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panel.add(new JLabel("Username: "), cs);

        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        tfUsername.addActionListener(onConfirmAction);
        panel.add(tfUsername, cs);

        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panel.add(new JLabel("Password: "), cs);

        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        pfPassword.addActionListener(onConfirmAction);
        panel.add(pfPassword, cs);
        panel.setBorder(new LineBorder(Color.GRAY));
        //endregion
        //region buttons
        JButton btnLogin = new JButton("Login");
        btnLogin.addActionListener(onConfirmAction);

        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(onCancelAction);
        JPanel bp = new JPanel();
        bp.add(btnLogin);
        bp.add(btnCancel);
        dialog.getContentPane().add(bp, BorderLayout.PAGE_END);
        //endregion

        dialog.getContentPane().add(panel, BorderLayout.CENTER);

        dialog.pack();
        dialog.setResizable(false);
        dialog.setModal(true);
        dialog.setVisible(true);
        //endregion
    }
}
