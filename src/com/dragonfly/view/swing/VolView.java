package com.dragonfly.view.swing;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.View;
import com.dragonfly.model.entity.LnkPersonneVol;
import com.dragonfly.model.entity.Vol;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class VolView extends View
{
    private JScrollPane createTable(Collection<Vol> vols)
    {
        VolTableModel tableModel = new VolTableModel();

        for(final Vol vol : vols) {
            for(final LnkPersonneVol lnkPersonneVol : vol.getEquipage())
            {
                tableModel.addRow(Arrays.asList(
                        vol.getId(),
                        Integer.toString(vol.getAvion().getId()),
                        Integer.toString(vol.getAvion().getModele().getId()),

                        vol.getDateDepart().toString(),
                        vol.getPisteDepart().getAirport().getName(),
                        vol.getDateArrivee().toString(),
                        vol.getPisteArrivee().getAirport().getName(),

                        lnkPersonneVol.getPersonne().getNom(),
                        lnkPersonneVol.getPersonne().getPrenom(),
                        lnkPersonneVol.getFonction().getLabel()
                ));
            }
        }

         JTable table = new JTable(tableModel);
         table.setPreferredScrollableViewportSize(new Dimension(500, 70));
         table.setFillsViewportHeight(true);
         return new JScrollPane(table);
    }

    @SuppressWarnings("unchecked")
    public void displayTemplate(ArgumentCollection args) throws SQLException
    {
        List<Vol> vols = (List<Vol>) args.get("vols");

        JFrame fenetre = new JFrame();
        fenetre.setTitle("Liste des vols");
        fenetre.setSize(400, 100);
        fenetre.setLocationRelativeTo(null);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        fenetre.add(this.createTable(vols));

        fenetre.setVisible(true);
    }

}
