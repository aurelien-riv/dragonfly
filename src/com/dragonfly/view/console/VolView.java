package com.dragonfly.view.console;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.kernel.View;
import com.dragonfly.model.entity.AirportPiste;
import com.dragonfly.model.entity.Avion;
import com.dragonfly.model.entity.LnkPersonneVol;
import com.dragonfly.model.entity.Vol;
import com.sun.org.apache.xpath.internal.Arg;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class VolView extends View
{
    static private MyScanner sc = new MyScanner();
    static private PrintStream out = System.out;

    @SuppressWarnings("unchecked")
    public void displayTemplate(ArgumentCollection args) throws SQLException {
        PrintStream out = System.out;
        List<Vol> vols = (List<Vol>) args.get("vols");

        for(Vol vol : vols)
        {
            for(LnkPersonneVol lnkPersonneVol : vol.getEquipage())
            {
                out.print(vol.getId() + "\t");
                out.print(vol.getAvion().getId() + "\t");
                out.print(vol.getAvion().getModele().getId() + "\t");

                out.print(vol.getDateDepart() + "\t");
                out.print(vol.getPisteDepart().getAirport().getName() + "\t");
                out.print(vol.getDateArrivee() + "\t");
                out.print(vol.getPisteArrivee().getAirport().getName() + "\t");

                out.print(lnkPersonneVol.getPersonne().getNom() + "\t");
                out.print(lnkPersonneVol.getPersonne().getPrenom() + "\t");
                out.print(lnkPersonneVol.getFonction().getLabel() + "\t");

                if (! vol.isValid())
                    out.print("/!\\");

                out.println();
            }
        }
    }

    public ArgumentCollection createOrUpdateFormTemplate(ArgumentCollection args) throws Throwable
    {
        Vol vol = (Vol) args.get("vol");

        String volId = vol.getId();
        if (volId == null)
            volId = sc.gets("Vol n° : ", "\\w+");
        else
            out.println("Vol n°" + vol.getId());

        return new ArgumentCollection(new Object[][] {
                {"volId", volId},
                {"avionId", getAvion(vol).getId()},
                {"pisteDepart", getPiste(vol.getPisteDepart(), "Piste de départ").getId()},
                {"pisteArrivee", getPiste(vol.getPisteDepart(), "Piste d'arrivée").getId()},
                {"dateDepart", sc.getDateTime("date et heure de départ [" + vol.getDateDepart() + "] : ")},
                {"dateArrivee", sc.getDateTime("date et heure d'arrivée [" + vol.getDateArrivee() + "] : ")}
        });
    }

    protected AirportPiste getPiste(AirportPiste current, String prompt) throws Throwable
    {
        if (current != null)
            out.println(current);
        while(true) {
            int piste = Integer.parseInt(sc.gets(prompt + " [" + (current != null ? current.getId() : null) + "] : ", "\\d+"));
            AirportPiste newPiste = (AirportPiste) Kernel.execute("AirportPiste", "get",
                    new ArgumentCollection(new Object[] {"airportPisteId", piste})).get("airportPiste");
            if (newPiste != null) {
                out.println(newPiste + " (" + newPiste.getAirport() + ")");
                return newPiste;
            }
        }
    }

    protected Avion getAvion(Vol vol) throws Throwable
    {
        Avion current = vol.getAvion();
        if (current != null)
            out.println(current);

        while (true) {
            int avion = Integer.parseInt(sc.gets("Avion [" + (current != null ? current.getId() : null) + "] : ", "\\d+"));
            Avion newAvion = (Avion) Kernel.execute("Avion", "get", new ArgumentCollection(new Object[] {"avionId", avion})).get("avion");
            if (newAvion != null) {
                out.println(newAvion);
                return newAvion;
            }
        }
    }
}
