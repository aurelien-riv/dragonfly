package com.dragonfly.view.console;

import lombok.Getter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MyScanner
{
    public static final String DATE_REGEX = "\\d{2}/\\d{2}/\\d{4}";
    public static final String DATETIME_REGEX = DATE_REGEX + " \\d{2}:\\d{2}:\\d{2}";

    @Getter
    Scanner sc = new Scanner(System.in);

    public String gets(String message, String regex)
    {
        Pattern pattern = Pattern.compile(regex);
        String s;
        do {
            System.out.print(message);
            s = sc.nextLine();
        } while (! pattern.matcher(s).matches());
        return s;
    }

    public Date getDate(String message) {
        while (true) {
            try {
                String dateStr = this.gets(message, DATE_REGEX);
                return new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
            } catch (ParseException ignored) {}
        }
    }

    public Date getDateTime(String message) {
        while (true) {
            try {
                String dateStr = this.gets(message, DATETIME_REGEX);
                return new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(dateStr);
            } catch (ParseException ignored) {}
        }
    }
}
