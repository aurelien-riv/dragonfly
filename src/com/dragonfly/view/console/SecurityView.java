package com.dragonfly.view.console;

import com.dragonfly.controller.SecurityController.InvalidCredentialsException;
import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.kernel.View;
import com.dragonfly.lib.PasswordEncoder;

import java.io.PrintStream;

public class SecurityView extends View
{
    public void loginFormTemplate(ArgumentCollection args) throws Throwable
    {
        MyScanner sc = new MyScanner();
        PrintStream out = System.out;

        while(true) {
            String login = sc.gets("login (or anonymous) : ", "\\w+");
            if (login.equals("anonymous"))
                return;
            String password = PasswordEncoder.hash(sc.gets("password :", ".*"));
            try {
                Kernel.execute("Security", "login", new ArgumentCollection(new Object[][]{
                        {"login",     login},
                        {"#password", password}
                }));
                return;
            } catch(InvalidCredentialsException e) {
                out.println("Login ou mot de passe invalide");
            }
        }
    }
}
