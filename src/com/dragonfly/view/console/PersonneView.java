package com.dragonfly.view.console;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.View;

public class PersonneView extends View
{
    public ArgumentCollection registerFormTemplate(ArgumentCollection args)
    {
        MyScanner sc = new MyScanner();
        String nom    = sc.gets("Nom : ", "[a-zA-Z -]{2,255}");
        String prenom = sc.gets("Nom : ", "[a-zA-Z -]{2,255}");

        return new ArgumentCollection(new Object[][] {
                {"nom", nom},
                {"prenom", prenom}
        });
    }
}
