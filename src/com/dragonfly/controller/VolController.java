package com.dragonfly.controller;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Controller;
import com.dragonfly.kernel.Request;
import com.dragonfly.model.ORM;
import com.dragonfly.model.dao.VolDao;
import com.dragonfly.model.entity.AirportPiste;
import com.dragonfly.model.entity.Avion;
import com.dragonfly.model.entity.Role;
import com.dragonfly.model.entity.Vol;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class VolController extends Controller
{
    @Template()
    @Security(anonymous = true)
    public ArgumentCollection displayAction(Request request) throws Exception
    {
        VolDao volDao = ORM.getDao(Vol.class);
        List<Vol> vols = volDao.find(0, 5, Vol.COL_DATE_DEPART, (Object[][]) request.getParams().get("filters"));

        return new ArgumentCollection(new Object[] {"vols", vols});
    }

    @Template
    @Security(roles = {Role.ROLE_ADMIN, Role.ROLE_MANAGER})
    public ArgumentCollection createOrUpdateFormAction(Request request) throws Exception
    {
        String volId = (String) request.getParams().get("volId");
        VolDao volDao = ORM.getDao(Vol.class);
        Vol vol = (volId == null ? new Vol() : volDao.queryForId(volId));
        return new ArgumentCollection(new Object[] {"vol", vol});
    }

    @Security(roles = {Role.ROLE_ADMIN})
    public ArgumentCollection createOrUpdateAction(Request request) throws Exception
    {
        ArgumentCollection args = request.getParams();
        VolDao volDao = ORM.getDao(Vol.class);
        Vol vol = volDao.queryForId((String) args.get("volId"));
        if (vol == null) {
            vol = new Vol();
            vol.setId((String) args.get("volId"));
        }
        Dao<Avion, Integer> avionDao = ORM.getDao(Avion.class);
        Dao<AirportPiste, Integer> airportPistesDao = ORM.getDao(AirportPiste.class);

        vol.setAvion(avionDao.queryForId((Integer) args.get("avionId")));
        vol.setPisteDepart (airportPistesDao.queryForId((Integer) args.get("pisteDepart")));
        vol.setPisteArrivee(airportPistesDao.queryForId((Integer) args.get("pisteArrivee")));
        vol.setDateDepart ((Date) args.get("dateDepart"));
        vol.setDateArrivee((Date) args.get("dateArrivee"));

        volDao.createOrUpdate(vol);
        return new ArgumentCollection(new Object[] {"vol", vol});
    }
}
