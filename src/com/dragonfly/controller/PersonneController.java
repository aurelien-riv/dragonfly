package com.dragonfly.controller;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Controller;
import com.dragonfly.kernel.Request;
import com.dragonfly.model.ORM;
import com.dragonfly.model.entity.Personne;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.query.In;

import java.util.regex.Pattern;

public class PersonneController extends Controller
{
    @Template
    @Security()
    public ArgumentCollection registerFormAction(Request request)
    {
        return null;
    }

    @Security()
    public ArgumentCollection registerAction(Request request) throws Exception
    {
        String nom    = (String) request.getParams().get("nom");
        String prenom = (String) request.getParams().get("prenom");

        Pattern pattern = Pattern.compile("[a-zA-Z -]{2,255}");
        if (! pattern.matcher(nom).matches() || ! pattern.matcher(prenom).matches())
            throw new IllegalArgumentException();

        Dao<Personne, Integer> personneDao = ORM.getDao(Personne.class);
        final Personne personne = new Personne(nom, prenom);
        personneDao.create(personne);

        return new ArgumentCollection(new Object[]{"personne", personne});
    }
}
