package com.dragonfly.controller;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Controller;
import com.dragonfly.kernel.Request;
import com.dragonfly.model.ORM;
import com.dragonfly.model.entity.Avion;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

public class AvionController extends Controller
{
    @Controller.Security(anonymous = true)
    public ArgumentCollection getAction(Request request) throws SQLException
    {
        Integer avionId = (Integer) request.getParams().get("avionId");
        Dao<Avion, Integer> avionDao = ORM.getDao(Avion.class);
        Avion avion = avionDao.queryForId(avionId);

        return new ArgumentCollection(new Object[] {"avion", avion});
    }
}
