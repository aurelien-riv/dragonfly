package com.dragonfly.controller;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Controller;
import com.dragonfly.kernel.Request;
import com.dragonfly.model.ORM;
import com.dragonfly.model.entity.Airport;
import com.dragonfly.model.entity.AirportPiste;
import com.dragonfly.model.entity.Role;
import com.j256.ormlite.dao.Dao;

import java.io.*;
import java.sql.SQLException;
import java.util.List;

public class AirportPisteController extends Controller
{
    @Controller.Security(anonymous = true)
    public ArgumentCollection getAction(Request request) throws SQLException
    {
        Integer pisteId = (Integer) request.getParams().get("airportPisteId");
        Dao<AirportPiste, Integer> airportPisteDao = ORM.getDao(AirportPiste.class);
        AirportPiste airportPiste = airportPisteDao.queryForId(pisteId);

        return new ArgumentCollection(new Object[] {"airportPiste", airportPiste});
    }

    public ArgumentCollection exportAction(Request request) throws SQLException, IOException
    {
        Dao<AirportPiste, Integer> airportPisteDao = ORM.getDao(AirportPiste.class);
        List<AirportPiste> airportPistes = airportPisteDao.queryForAll();

        FileOutputStream fos= new FileOutputStream("airports.ser");
        ObjectOutputStream oos= new ObjectOutputStream(fos);
        oos.writeObject(airportPistes);
        oos.close();
        fos.close();

        return new ArgumentCollection(new Object[] {"file", "airports.ser"});
    }

    @SuppressWarnings("unchecked")
    @Controller.Security(roles = {Role.ROLE_ADMIN})
    public ArgumentCollection importAction(Request request) throws Exception
    {
        String path = (String) request.getParams().get("file");

        FileInputStream fos= new FileInputStream(path);
        ObjectInputStream oos= new ObjectInputStream(fos);
        List<AirportPiste> airportPistes = (List<AirportPiste>) oos.readObject();
        oos.close();
        fos.close();

        Dao<Airport,      Integer> airportDao      = ORM.getDao(Airport.class);
        Dao<AirportPiste, Integer> airportPisteDao = ORM.getDao(AirportPiste.class);
        for(AirportPiste airportPiste : airportPistes) {
            airportDao.createOrUpdate(airportPiste.getAirport());
            airportPisteDao.createOrUpdate(airportPiste);
        }
        return null;
    }
}
