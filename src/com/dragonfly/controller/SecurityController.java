package com.dragonfly.controller;

import com.dragonfly.kernel.ArgumentCollection;
import com.dragonfly.kernel.Controller;
import com.dragonfly.kernel.Kernel;
import com.dragonfly.kernel.Request;
import com.dragonfly.kernel.Security.AccessDeniedException;
import com.dragonfly.lib.PasswordEncoder;
import com.dragonfly.model.ORM;
import com.dragonfly.model.dao.UserDao;
import com.dragonfly.model.entity.User;

import java.sql.SQLException;

public class SecurityController extends Controller
{
    public class InvalidCredentialsException extends Exception
    {

    }

    @Template
    @Security(anonymous = true)
    public ArgumentCollection loginFormAction(Request request) throws AccessDeniedException
    {
        if (request.getUser() != null)
            throw new AccessDeniedException("You are already authenticated");
        return null;
    }

    @Security(anonymous = true)
    public ArgumentCollection loginAction(Request request) throws InvalidCredentialsException, SQLException
    {
        ArgumentCollection params = request.getParams();

        String login = (String) params.get("login");
        String password = (String) params.get("#password");
        if (password == null)
            password = PasswordEncoder.hash((String) params.get("password"));

        UserDao userDao = ORM.getDao(User.class);
        User user = userDao.findByLoginAndPassword(login, password);
        if (user == null)
            throw new InvalidCredentialsException();
        Kernel.setUser(user);
        return null;
    }

    public ArgumentCollection logoutAction(Request request)
    {
        Kernel.setUser(null);
        return null;
    }
}
