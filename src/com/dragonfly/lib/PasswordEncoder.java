package com.dragonfly.lib;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordEncoder
{
    public static final String ALGORITHM = "SHA-512";

    public static String hash(String password)
    {
        try {
            byte[] bytes = MessageDigest.getInstance(ALGORITHM).digest(password.getBytes());

            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) { // Should never be reached
            e.printStackTrace();
            return "";
        }
    }
}
