package com.dragonfly.kernel;

public class FrameworkError extends Error
{
    public FrameworkError(Throwable cause) {
        super(cause);
    }
}
