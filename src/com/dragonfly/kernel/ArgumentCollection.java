package com.dragonfly.kernel;

import java.util.HashMap;

public class ArgumentCollection extends HashMap<String, Object>
{
    public ArgumentCollection(Object[] ... args)
    {
        super();
        for(Object[] arg : args) {
            this.put((String) arg[0], arg[1]);
        }
    }
}
