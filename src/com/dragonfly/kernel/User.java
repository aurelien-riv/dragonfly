package com.dragonfly.kernel;

import java.util.Collection;

public interface User {
    Collection<String> getRoles();
}
