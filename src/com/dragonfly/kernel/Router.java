package com.dragonfly.kernel;

import java.lang.reflect.Method;

class Router
{
    // TODO Move to an initializer method or a configuration manager
    public static String BASE_PKG = "com.dragonfly";
    public static String CONTROLLER_PKG = BASE_PKG + ".controller.";
    public static String VIEW_PKG = BASE_PKG + ".view.swing.";

    protected static Controller getController(String module) throws Exception
    {
        Class<?> controllerClass = Class.forName(CONTROLLER_PKG + module + "Controller");
        return (Controller) controllerClass.getConstructor().newInstance();
    }

    public static Method getAction(Controller controller, String action) throws Exception
    {
        return controller.getClass().getMethod(action+"Action", Request.class);
    }

    protected static View getView(String module) throws Exception
    {
        Class<?> viewClass = Class.forName(VIEW_PKG + module + "View");
        return (View) viewClass.getConstructor().newInstance();
    }

    public static Method getTemplate(View view, String action) throws Exception
    {
        return view.getClass().getMethod(action+"Template", ArgumentCollection.class);
    }
}
