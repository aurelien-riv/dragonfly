package com.dragonfly.kernel;

import lombok.Getter;
import lombok.Setter;

@Getter  @Setter
public class Request
{
    private User user;

    private ArgumentCollection params;

    public Request(User user, ArgumentCollection params)
    {
        this.user = user;
        this.params = (params != null ? params : new ArgumentCollection());
    }
}
