package com.dragonfly.kernel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Kernel
{
    // TODO make multi user
    private static User user = null;

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        Kernel.user = user;
    }

    public static ArgumentCollection execute(String module, String action, ArgumentCollection args) throws Throwable
    {
        try {
            Controller controller = Router.getController(module);
            Method actionMethod = Router.getAction(controller, action);

            Security security = new Security(user, actionMethod);
            security.checkGranted(args);

            Request request = new Request(Kernel.user, args);
            ArgumentCollection response = (ArgumentCollection) actionMethod.invoke(controller, request);

            Controller.Template a_template = actionMethod.getAnnotation(Controller.Template.class);
            if (a_template != null)
            {
                View view = Router.getView(module);
                return (ArgumentCollection) Router.getTemplate(view, action).invoke(view, response);
            }
            else
                return response;
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof ReflectiveOperationException)
                throw new FrameworkError(e.getCause());
           throw e.getCause();
        }
    }
}
