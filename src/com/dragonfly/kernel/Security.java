package com.dragonfly.kernel;

import java.lang.reflect.Method;
import java.util.Collection;

import static com.dragonfly.kernel.Controller.Security.Strategy;
import static com.dragonfly.kernel.Controller.Security.ObjectPermission;

public class Security
{
    public static class AccessDeniedException extends Exception
    {
        public AccessDeniedException(String message) {
            super(message);
        }
    }

    private User user;
    private Controller.Security a_security;

    public Security(User user, Method action) {
        this.user = user;
        this.a_security = action.getAnnotation(Controller.Security.class);
    }

    public void checkGranted(ArgumentCollection args) throws Exception
    {
        this.checkGrantedAction();
        this.checkGrantedObjects(args);
    }

    public void checkGrantedAction() throws AccessDeniedException
    {
        if (a_security == null) {
            if (user == null)
                throw new AccessDeniedException("You are not authenticated");
        } else {
            if (! a_security.anonymous()) {
                if (user == null)
                    throw new AccessDeniedException("You are not authenticated");
                if (a_security.roles().length > 0)
                {
                    boolean has_one = false;
                    boolean needs_one = a_security.strategy() == Strategy.HAS_ONE;
                    boolean needs_all = a_security.strategy() == Strategy.HAS_ALL;
                    Collection<String> user_roles = user.getRoles();

                    for (String role : a_security.roles()) {
                        if (needs_one && user_roles.contains(role)) {
                            has_one = true;
                            break;
                        }
                        if (needs_all && ! user_roles.contains(role))
                            throw new AccessDeniedException("You don't have all the required roles");
                    }
                    if (! has_one && needs_one)
                        throw new AccessDeniedException("You don't have at least one of the required roles");
                }
            }
        }
    }


    private void checkGrantedObjects(ArgumentCollection args) throws AccessDeniedException, Exception
    {
        if (a_security == null)
            return;

        for (ObjectPermission permission : a_security.permissions()) {
            Object obj = args.get(permission.object());
            String perm = (String) args.get(permission.permission());
            Voter.Security a_security_voter = obj.getClass().getAnnotation(Voter.Security.class);

            Voter<?> voter = (Voter<?>) a_security_voter.voterClass().getDeclaredConstructor(obj.getClass()).newInstance(obj);
            Method isGranted = voter.getClass().getMethod("isGranted", User.class, String.class);
            if (! (Boolean) isGranted.invoke(voter, this.user, perm))
                throw new AccessDeniedException("You don't have the required permissions on an object");
        }
    }
}
