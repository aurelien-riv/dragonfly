package com.dragonfly.kernel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class Controller
{
    /**
     * Tell the Kernel whether the action has a template
     */
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Template
    {
    }

    /**
     * Grant or deny access to an action according to the user's roles
     */
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Security
    {
        @interface ObjectPermission
        {
            String object();
            String permission();
        }

        enum Strategy {HAS_ONE, HAS_ALL};

        String[] roles()    default {};
        Strategy strategy() default Strategy.HAS_ONE;

        ObjectPermission[] permissions() default {};

        /**
         * If true, not logged in users can run the action
         */
        boolean anonymous() default false;
    }
}
