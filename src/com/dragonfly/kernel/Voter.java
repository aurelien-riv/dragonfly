package com.dragonfly.kernel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class Voter<T>
{
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Security {
        Class<?> voterClass();
    }

    protected T object;

    public Voter(T object)
    {
        this.object = object;
    }

    public abstract boolean isGranted(User user, String action);
}
